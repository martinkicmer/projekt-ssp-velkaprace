# Projekt Evidence pacientů

## Odkaz na stránky

Zde je [odkaz](https://projekt-ssp-velkaprace-martinkicmer-2f71a7d1b2cc1bbc94868aae22b.gitlab.io/projektos/index.html) na stránky

## Popis

Tento projekt je webová aplikace, která byla vytvořena do předmětu URO.

Demonstruje především designovou stránku věci.

Dále základní funkcionalitu jako například přepínání mezi stránkami.

Webová aplikace obsahuje HTML, CSS a JavaScript soubory.

## Použití

- Naklonování repozitáře.
- Otevřít si složku s názvem "projekt".
- Lokálně si otevřít ve Vašem prohlížeči soubor "index.html".
